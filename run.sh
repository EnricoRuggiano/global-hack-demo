#!/bin/sh

# endpoint
docker run -p 1234:1234 -d hackaton/endpoint

# database
docker run -p 5432:5432 -d hackaton/database

# frontend
docker run -p 80:80 -d hackaton/frontend 
