#!/bin/sh

docker build -t hackaton/endpoint endpoint/
echo "DOCKER:\t built endpoint"

docker build -t hackaton/database database/
echo "DOCKER:\t built database"

docker build -t hackaton/frontend frontend/
echo "DOCKER:\t built frontend"
