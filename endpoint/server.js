'use strict';

// external libs
const express = require('express');
const { Client } = require('pg');
const dbInit = require('./src/dbInit');
const { pool } = require('./src/config');

// Constants
const PORT = 1234;
const HOST = '0.0.0.0';

// HTTP Get
const app = express();
app.get('/sample_providers', (req, res) => {
  res.sendFile(__dirname + '/csv/sample_providers.csv');
});


// GET providers
app.get('/get_providers', (req, res) => {
  pool.query("SELECT * FROM providers", (ps_err, ps_res) => {
    if (ps_err)
    {
      // show on Endpoint error
      console.log(ps_err);

      // notify to client
      res.send(ps_err);      
    }
    else
    {
      // result of query of the db as a string
      let strJson = JSON.stringify(ps_res);
      console.log(strJson);
      // send to the client
      res.send(ps_res);
    }
  });
});


// init posgre db
dbInit();

// HTTP listen
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

