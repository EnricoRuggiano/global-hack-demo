var pgtools = require('pgtools');
var { pool } = require("./config"); 

// CONSTANTS
const db = "hackaton";
const config = {
  user: 'docker', // lets connect to db as admin :)
  password: 'docker',
  port: 5432,
  host: 'localhost'
};

// TABLES
// CUSTOMERS
var tableCustomer = function()
{
  pool.query(
    'CREATE TABLE IF NOT EXISTS customers (' +
    'id varchar(45) NOT NULL,' +
    'username varchar(45),' +
    'password varchar(450),' +
    'email varchar(450),' + 
    'PRIMARY KEY (id))'
    , (err, res) => {
    if (err) {
      console.log("CREATE CUSTOMER TABLE FAILED");
      console.log(err);
    }
    else {
      console.log("CREATE CUSTOMER TABLE OK");
    }
  });
};

// PROVIDERS
var tableProviders = function()
{
  pool.query(
    'CREATE TABLE IF NOT EXISTS providers (' +
    'vat_n varchar(45),' +
    'username varchar(45),' +
    'category varchar(45),' +
    'location varchar(45),' +
    'num_services numeric,' +
    'password varchar(450),' +
    'email varchar(450),' +
    'PRIMARY KEY (vat_n))'
    , (err, res) => {
    if (err) {
      console.log("CREATE PROVIDERS TABLE FAILED");
      console.error(err);
    }
    else {
      console.log("CREATE PROVIDERS TABLE OK");
    }
  });
};

// SERVICES
var tableServices = function()
{
  pool.query(
    'CREATE TABLE IF NOT EXISTS services (' +
    'id varchar(45) NOT NULL,' +
    'vat_n_provider varchar(45),' +
    'category varchar(45),' +
    'cost numeric,' +
    'PRIMARY KEY (id))'
    , (err, res) => {
    if (err) {
      console.log("CREATE SERVICES TABLE FAILED");
      console.error(err);
    }
    else {
      console.log("CREATE SERVICES TABLE OK");
    }
  });
};


// BILLS
var tableBills = function()
{
  pool.query(
    'CREATE TABLE IF NOT EXISTS bills (' +
    'vat_n_provider varchar(45),' +
    'id_user varchar(45),' +
    'id_service varchar(45),' +
    'PRIMARY KEY (vat_n_provider, id_user, id_service))'
    , (err, res) => {
    if (err) {
      console.log("CREATE BILLS TABLE FAILED");
      console.error(err);
    }
    else {
      console.log("CREATE BILLS TABLE OK");
    }
  });
};

// FILL TABLES
var sampleProviders = function()
{
  pool.query(
    "COPY providers(vat_n, username, category, location, " +
    "num_services, password, email)" +
    "FROM '/data/sample_providers.csv' DELIMITER ',' CSV HEADER",
    (err, res) => {
      if (err){
        console.log("COPY PROVIDERS CSV FAILED");
        console.error(err);
      }
      else{
        console.log("IMPORTED SAMPLE PROVIDERS OK");
      }
    }
  )
}


// CREATE DATABASE
var dbInit = function()
{
  pgtools.createdb(config, db, function(err, res) {
    if (err) {
      console.error(err);
    }
    console.log("CREATE DB OK");

    // CREATE customer table
    tableCustomer();
    // CREATE providers table
    tableProviders();
    // CREATE services table
    tableServices();
    // CREATE bills table
    tableBills();

    // COPY PROVIDERS CSV
    sampleProviders();
  });
}



module.exports = dbInit;