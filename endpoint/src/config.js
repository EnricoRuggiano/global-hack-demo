const { Pool } = require('pg');

// lets connect to db as admin :)
const DB_USER = "docker"; 
const DB_PASSWORD = "docker";
const DB_HOST = "localhost";
const DB_PORT = 5432;
const DB_DATABASE = "hackaton";

const url = `postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}`

const pool = new Pool({
  connectionString: url
});

module.exports = { pool }