
function get_popup(Mappa, lat, long) {

  var myLocation = new OpenLayers.Geometry.Point(lat, long)
      .transform('EPSG:4326', 'EPSG:3857');

  var popup = new OpenLayers.Popup.FramedCloud("Popup",
      myLocation.getBounds().getCenterLonLat(), null,
      'Il mercatino di Zorma<br>Categoria: Food & Beverage<br><a href="./map.html">Visualizza offerte</a>', null,
      true // <-- true if we want a close (X) button, false otherwise
  );

  Mappa.addPopup(popup);

}

// Definisco la variabilie mappa come un oggetto OpenLayers.Map utilizzando il DivMappa, poi aggiungo il Layer OSM (Open Street Map)
var Mappa = new OpenLayers.Map("DivMappa");
Mappa.addLayer(new OpenLayers.Layer.OSM());

// Creo l'oggetto contenente le coordinate (prima longitudine e poi latitudine)
var LonLat = new OpenLayers.LonLat( 9.186,  45.475 )
      .transform(
        new OpenLayers.Projection("EPSG:4326"), // Transformazione da WGS 1984..
        Mappa.getProjectionObject() // .. a Spherical Mercator Projection
      );

var LonLat1 = new OpenLayers.LonLat( 9.188,  45.477 )
      .transform(
        new OpenLayers.Projection("EPSG:4326"), // Transformazione da WGS 1984..
        Mappa.getProjectionObject() // .. a Spherical Mercator Projection
      );

var LonLat2 = new OpenLayers.LonLat( 9.185,  45.472 )
      .transform(
        new OpenLayers.Projection("EPSG:4326"), // Transformazione da WGS 1984..
        Mappa.getProjectionObject() // .. a Spherical Mercator Projection
      );

// Imposto lo zoom
var zoom=16;

// Imposto le coordinate di lonLat come centro della mappa di partenza
Mappa.setCenter(LonLat, zoom);
